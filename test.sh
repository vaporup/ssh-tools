#!/usr/bin/env bash

set -euo pipefail

TEST_SSH_USER="root"
TEST_SSH_SERVER="192.168.1.10"

SCRIPT_PATH="$(dirname "${BASH_SOURCE[0]}")"
COMMAND_PATH="${SCRIPT_PATH}/cmd/bash"

${COMMAND_PATH}/ssh-keyinfo/ssh-keyinfo ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo -v ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo -c ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo -cv ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo -c -w 20000 ${SCRIPT_PATH}/examples/*/*
${COMMAND_PATH}/ssh-certinfo/ssh-certinfo -cv -w 20000 ${SCRIPT_PATH}/examples/*/*
CHECK_REMOTE_FILE_EXISTS=NO sshpass -e ${COMMAND_PATH}/ssh-diff/ssh-diff /etc/hosts ${TEST_SSH_USER}@${TEST_SSH_SERVER}
sshpass -e ${COMMAND_PATH}/ssh-facts/ssh-facts ${TEST_SSH_USER}@${TEST_SSH_SERVER}
${COMMAND_PATH}/ssh-hostkeys/ssh-hostkeys ${TEST_SSH_SERVER}
sshpass -e ${COMMAND_PATH}/ssh-ping/ssh-ping -4 -v -c 3 -D ${TEST_SSH_USER}@${TEST_SSH_SERVER}
sshpass -e ${COMMAND_PATH}/ssh-ping/ssh-ping -4 -v -c 3 -H ${TEST_SSH_USER}@${TEST_SSH_SERVER}
SSH_PING_NO_COLORS=true sshpass -e ${COMMAND_PATH}/ssh-ping/ssh-ping -4 -v -c 3 -H ${TEST_SSH_USER}@${TEST_SSH_SERVER}
${COMMAND_PATH}/ssh-version/ssh-version ${TEST_SSH_SERVER}

shellcheck ${COMMAND_PATH}/*/ssh-*

printf "\n**** Finished *****\n\n"
