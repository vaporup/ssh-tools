#  EXAMPLES

```bash
  ssh-sig sign file.txt
  ssh-sig check -s file.txt.sig < file.txt

  cat file.txt | ssh-sig sign   > file.txt.sig
  cat file.txt | ssh-sig check -s file.txt.sig

  # Sign multiple files
  ssh-sig sign file1.txt file2.txt [...]

  # Use another key for signing
  ssh-sig sign -f ~/.ssh/id_rsa file.txt

  # Use a public key. Private Key for signing then comes from ssh-agent
  ssh-sig sign -f ~/.ssh/id_rsa.pub file.txt
```

# Info

- https://www.agwa.name/blog/post/ssh_signatures
- https://superuser.com/a/1616656
- https://seankhliao.com/blog/12020-01-09-signing-and-encrypting/
- https://github.com/hiddeco/sshsig
  - https://github.com/hiddeco/sshsig/pull/7
