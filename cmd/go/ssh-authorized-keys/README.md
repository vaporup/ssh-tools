Collects info from **authorized_keys** files from every user it can find

## Usage

```
 Usage: ssh-authorized-keys [OPTION] [<file> ...]

 Collects info from authorized_keys files from every user it can find

 OPTIONS

 -j  --json     output JSON
 -J  --jsonl    output JSONLINES
 -r  --rec      output RECUTILS
 -x  --xml      output XML
 -y  --yaml     output YAML

 --version  show version information

 ARGUMENTS

 file        authorized_keys file
```

## ~/.ssh/authorized_keys

```
# foo bar
  # comment
# another comment
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN5EYh69EeIDiVYxgy6O4fUWoxiT4cRxb8JkLdikE27a sven.wick@gmx.de
cert-authority,no-touch-required,principals="user_a" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPIIlIm+khfFsXJIaVVJRwQ30YmXin4cg1dNx0ISnFaa cert-test # foo bar

ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN5EYh69EeIDiVYxgy6O4fUWoxiT4cRxb8JkLdikE27a o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICebzmkN9HFcnTfcugV+8sAC1nI9itIL0uriy8lmTc7L o:user2@host2|p:manage-users
```

### DEFAULT Output

```bash
ssh-authorized-keys
```

```
Name                 : swick
Directory            : /home/swick
File                 : /home/swick/.ssh/authorized_keys
Type                 : ssh-ed25519
FingerprintMD5       : e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
FingerprintSHA256    : SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
Comment              : sven.wick@gmx.de
Options              : []
IsCA                 : false

Name                 : swick
Directory            : /home/swick
File                 : /home/swick/.ssh/authorized_keys
Type                 : ssh-ed25519
FingerprintMD5       : 4d:eb:ad:b1:c6:8b:f6:ce:91:97:c8:44:71:82:57:10
FingerprintSHA256    : SHA256:dTok9sXQhX8z9SuX1Q4b7sXZWo59nR3OAUbUoy/tSlE
Comment              : cert-test # foo bar
Options              : [cert-authority no-touch-required principals="user_a"]
IsCA                 : true

Name                 : swick
Directory            : /home/swick
File                 : /home/swick/.ssh/authorized_keys
Type                 : ssh-ed25519
FingerprintMD5       : e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
FingerprintSHA256    : SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
Comment              : o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21
Options              : []
IsCA                 : false

Name                 : swick
Directory            : /home/swick
File                 : /home/swick/.ssh/authorized_keys
Type                 : ssh-ed25519
FingerprintMD5       : 65:bf:6e:db:35:2b:e6:2b:27:31:3a:71:52:e8:40:df
FingerprintSHA256    : SHA256:WwI/9m0ofHGt2hdHRNKAdRuaqkJj5KlV4ePx5nEyglE
Comment              : o:user2@host2|p:manage-users
Options              : []
IsCA                 : false
```

### DEFAULT Output (Get Origin from Comments)

Checkout the comment format from this discussion: https://news.ycombinator.com/item?id=39470798

```bash
ssh-authorized-keys \
| awk '$NF ~ /^.:/ {print $NF}' \
| tr '|' '\n' \
| awk -F':' '/^o:/ {print "origin="$2}'
```

```
origin=user1@host1
origin=user2@host2
```

### JSON Output

```bash
ssh-authorized-keys -j
```

```json
[
    {
        "Name": "swick",
        "Directory": "/home/swick",
        "File": "/home/swick/.ssh/authorized_keys",
        "Type": "ssh-ed25519",
        "FingerprintMD5": "e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8",
        "FingerprintSHA256": "SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0",
        "Comment": "sven.wick@gmx.de",
        "Options": null,
        "IsCA": false
    },
    {
        "Name": "swick",
        "Directory": "/home/swick",
        "File": "/home/swick/.ssh/authorized_keys",
        "Type": "ssh-ed25519",
        "FingerprintMD5": "4d:eb:ad:b1:c6:8b:f6:ce:91:97:c8:44:71:82:57:10",
        "FingerprintSHA256": "SHA256:dTok9sXQhX8z9SuX1Q4b7sXZWo59nR3OAUbUoy/tSlE",
        "Comment": "cert-test # foo bar",
        "Options": [
            "cert-authority",
            "no-touch-required",
            "principals=\"user_a\""
        ],
        "IsCA": true
    },
    {
        "Name": "swick",
        "Directory": "/home/swick",
        "File": "/home/swick/.ssh/authorized_keys",
        "Type": "ssh-ed25519",
        "FingerprintMD5": "e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8",
        "FingerprintSHA256": "SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0",
        "Comment": "o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21",
        "Options": null,
        "IsCA": false
    },
    {
        "Name": "swick",
        "Directory": "/home/swick",
        "File": "/home/swick/.ssh/authorized_keys",
        "Type": "ssh-ed25519",
        "FingerprintMD5": "65:bf:6e:db:35:2b:e6:2b:27:31:3a:71:52:e8:40:df",
        "FingerprintSHA256": "SHA256:WwI/9m0ofHGt2hdHRNKAdRuaqkJj5KlV4ePx5nEyglE",
        "Comment": "o:user2@host2|p:manage-users",
        "Options": null,
        "IsCA": false
    }
]
```

### YAML Output

```bash
ssh-authorized-keys -y
```

```yaml
- Comment: sven.wick@gmx.de
  File: /home/swick/.ssh/authorized_keys
  FingerprintMD5: e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
  FingerprintSHA256: SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
  Directory: /home/swick
  IsCA: false
  Options: null
  Type: ssh-ed25519
  Name: swick
- Comment: 'cert-test # foo bar'
  File: /home/swick/.ssh/authorized_keys
  FingerprintMD5: 4d:eb:ad:b1:c6:8b:f6:ce:91:97:c8:44:71:82:57:10
  FingerprintSHA256: SHA256:dTok9sXQhX8z9SuX1Q4b7sXZWo59nR3OAUbUoy/tSlE
  Directory: /home/swick
  IsCA: true
  Options:
  - cert-authority
  - no-touch-required
  - principals="user_a"
  Type: ssh-ed25519
  Name: swick
- Comment: o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21
  File: /home/swick/.ssh/authorized_keys
  FingerprintMD5: e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
  FingerprintSHA256: SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
  Directory: /home/swick
  IsCA: false
  Options: null
  Type: ssh-ed25519
  Name: swick
- Comment: o:user2@host2|p:manage-users
  File: /home/swick/.ssh/authorized_keys
  FingerprintMD5: 65:bf:6e:db:35:2b:e6:2b:27:31:3a:71:52:e8:40:df
  FingerprintSHA256: SHA256:WwI/9m0ofHGt2hdHRNKAdRuaqkJj5KlV4ePx5nEyglE
  Directory: /home/swick
  IsCA: false
  Options: null
  Type: ssh-ed25519
  Name: swick
```

### RECUTILS Output

```bash
ssh-authorized-keys -r
```

```
Name: swick
Directory: /home/swick
File: /home/swick/.ssh/authorized_keys
Type: ssh-ed25519
FingerprintMD5: e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
FingerprintSHA256: SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
Comment: sven.wick@gmx.de
Options: []
IsCA: false

Name: swick
Directory: /home/swick
File: /home/swick/.ssh/authorized_keys
Type: ssh-ed25519
FingerprintMD5: 4d:eb:ad:b1:c6:8b:f6:ce:91:97:c8:44:71:82:57:10
FingerprintSHA256: SHA256:dTok9sXQhX8z9SuX1Q4b7sXZWo59nR3OAUbUoy/tSlE
Comment: cert-test # foo bar
Options: [cert-authority no-touch-required principals="user_a"]
IsCA: true

Name: swick
Directory: /home/swick
File: /home/swick/.ssh/authorized_keys
Type: ssh-ed25519
FingerprintMD5: e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8
FingerprintSHA256: SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0
Comment: o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21
Options: []
IsCA: false

Name: swick
Directory: /home/swick
File: /home/swick/.ssh/authorized_keys
Type: ssh-ed25519
FingerprintMD5: 65:bf:6e:db:35:2b:e6:2b:27:31:3a:71:52:e8:40:df
FingerprintSHA256: SHA256:WwI/9m0ofHGt2hdHRNKAdRuaqkJj5KlV4ePx5nEyglE
Comment: o:user2@host2|p:manage-users
Options: []
IsCA: false
```

### RECUTILS Output (Only Comments)

```bash
ssh-authorized-keys -r | recsel -Cp Comment
```

```
Comment: sven.wick@gmx.de
Comment: cert-test # foo bar
Comment: o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21
Comment: o:user2@host2|p:manage-users
```

### XML Output

```bash
ssh-authorized-keys -x
```

```xml
<AuthorizedKey>
    <Name>swick</Name>
    <Directory>/home/swick</Directory>
    <File>/home/swick/.ssh/authorized_keys</File>
    <Type>ssh-ed25519</Type>
    <FingerprintMD5>e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8</FingerprintMD5>
    <FingerprintSHA256>SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0</FingerprintSHA256>
    <Comment>sven.wick@gmx.de</Comment>
    <IsCA>false</IsCA>
</AuthorizedKey>
<AuthorizedKey>
    <Name>swick</Name>
    <Directory>/home/swick</Directory>
    <File>/home/swick/.ssh/authorized_keys</File>
    <Type>ssh-ed25519</Type>
    <FingerprintMD5>4d:eb:ad:b1:c6:8b:f6:ce:91:97:c8:44:71:82:57:10</FingerprintMD5>
    <FingerprintSHA256>SHA256:dTok9sXQhX8z9SuX1Q4b7sXZWo59nR3OAUbUoy/tSlE</FingerprintSHA256>
    <Comment>cert-test # foo bar</Comment>
    <Options>cert-authority</Options>
    <Options>no-touch-required</Options>
    <Options>principals=&#34;user_a&#34;</Options>
    <IsCA>true</IsCA>
</AuthorizedKey>
<AuthorizedKey>
    <Name>swick</Name>
    <Directory>/home/swick</Directory>
    <File>/home/swick/.ssh/authorized_keys</File>
    <Type>ssh-ed25519</Type>
    <FingerprintMD5>e8:c6:8b:cc:0f:b2:db:0c:e4:75:70:d7:80:e1:bb:c8</FingerprintMD5>
    <FingerprintSHA256>SHA256:G7h9i5+NDU72Ae40gCkxyvDz/8BH+KETw7sXHCYr5w0</FingerprintSHA256>
    <Comment>o:user1@host1|e:user1@domain.com|p:manage-vhosts|t:2024-02-21</Comment>
    <IsCA>false</IsCA>
</AuthorizedKey>
<AuthorizedKey>
    <Name>swick</Name>
    <Directory>/home/swick</Directory>
    <File>/home/swick/.ssh/authorized_keys</File>
    <Type>ssh-ed25519</Type>
    <FingerprintMD5>65:bf:6e:db:35:2b:e6:2b:27:31:3a:71:52:e8:40:df</FingerprintMD5>
    <FingerprintSHA256>SHA256:WwI/9m0ofHGt2hdHRNKAdRuaqkJj5KlV4ePx5nEyglE</FingerprintSHA256>
    <Comment>o:user2@host2|p:manage-users</Comment>
    <IsCA>false</IsCA>
</AuthorizedKey>
```
