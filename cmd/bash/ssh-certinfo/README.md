## ssh-certinfo
Shows validity and information of SSH certificates

  ~~~~
  $ ssh-certinfo ~/.ssh/*.pub

  /home/vaporup/.ssh/id_rsa-cert.pub  SSH_CERT_VALID    forever              ->  forever
  /home/vaporup/.ssh/test1-cert.pub   SSH_CERT_INVALID  2038-01-19T04:14:07  ->  2038-01-19T04:14:07
  /home/vaporup/.ssh/test2-cert.pub   SSH_CERT_EXPIRED  1988-11-14T13:36:40  ->  1991-08-11T14:36:40
  ~~~~

  ~~~~
  $ ssh-certinfo -v ~/.ssh/*.pub

  /home/vaporup/.ssh/id_rsa-cert.pub:
          Type: ssh-rsa-cert-v01@openssh.com user certificate
          Public key: RSA-CERT SHA256:Mm7o312345YEaWetVshTBslX48h0XJceLWzxx3RugDg
          Signing CA: RSA SHA256:4fcOpOm/Xk12345mYnihk0cr6SdjghPgONxriMJex+A
          Key ID: "vaporup"
          Serial: 0
          Valid: forever
          Principals: (none)
          Critical Options: (none)
          Extensions:
                  permit-X11-forwarding
                  permit-agent-forwarding
                  permit-port-forwarding
                  permit-pty
                  permit-user-rc
  ~~~~
