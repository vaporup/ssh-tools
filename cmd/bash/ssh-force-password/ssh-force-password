#!/usr/bin/env bash

# +---------------------------------------------------------------------------------------------------------------+
# | Title        : ssh-force-password                                                                             |
# | Description  : Enforces password authentication (as long as the server allows it)                             |
# |                It became quite annoying googling the SSH options for this every time                          |
# | Author       : Sven Wick <sven.wick@gmx.de>                                                                   |
# | URL          : https://codeberg.org/vaporup/ssh-tools                                                         |
# | Based On     : https://www.cyberciti.biz/faq/howto-force-ssh-client-login-to-use-only-password-authentication |
# +---------------------------------------------------------------------------------------------------------------+

VERSION="ssh-force-password (ssh-tools) dev"

# shellcheck disable=SC2029

ssh_opts=(
    -o "PreferredAuthentications=password"
    -o "PubkeyAuthentication=no"
)

#
# Usage/Help message
#

function usage() {

cat << EOF

    Usage: ${0##*/} [DEFAULT SSH OPTIONS] hostname

    Enforces password authentication (for password testing)
EOF

}

if [[ -z $1 || $1 == "--help" ]]; then
    usage
    exit 1
fi

if [[ $1 == "--version" ]]; then
    echo "${VERSION}"
    exit
fi

ssh "${ssh_opts[@]}" "$@"
