## ssh-keyinfo
Prints keys in several formats

  ~~~~
  $ ssh-keyinfo ~/.ssh/*.pub

  (RSA)  2048     MD5 40:52:aa:14:a5:9a:fe:b5:96:c0:d2:dc:99:f1:a5:77  /home/vaporup/.ssh/id_rsa.pub
  (RSA)  2048  SHA256 V4F6ipY4gG83sQGM0eRk0+g1RWE9K4asDUj1jFpCa48      /home/vaporup/.ssh/id_rsa.pub
  ~~~~
