## ssh-hostkeys
Prints server host keys in several formats

  ~~~~
  $ ssh-hostkeys kim

    (ECDSA)   256     MD5 3d:8e:49:6e:40:34:c6:e4:c8:5f:60:d9:ad:bf:1f:b9
    (ECDSA)   256  SHA256 txYpmPENBWolKBaA5EkCM/y1Hm2DP81jtABFrWUBf/c
  (ED25519)   256     MD5 a8:93:71:f9:13:d0:32:02:c0:e5:1e:f6:b9:db:8c:b0
  (ED25519)   256  SHA256 JZMLzNS2ifdGYv2SrqDodz7+/qaRCTBPukaCnMQqbX8
      (RSA)  2048     MD5 8b:37:63:cd:58:fd:9c:18:93:08:3a:4c:84:64:87:91
      (RSA)  2048  SHA256 WkOzD8BaeljejgTTAwECkByyb4kHdI0pnJB2/gcS1Lk
  ~~~~
