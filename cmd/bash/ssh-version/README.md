## ssh-version
Shows version of the SSH server you are connecting to

  ~~~~
  $ ssh-version kim

  Remote protocol version 2.0, remote software version OpenSSH_7.2p2 Ubuntu-4ubuntu2.6
  ~~~~

### Demo

![](demo.gif)
