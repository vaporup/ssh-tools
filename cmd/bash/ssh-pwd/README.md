## ssh-pwd
quickly echo path to use for scp, rsync

  ~~~~
  $ ssh-pwd

  vaporup@10.159.33.134:/home/vaporup/
  vaporup@192.168.1.102:/home/vaporup/
  vaporup@192.168.122.1:/home/vaporup/
  vaporup@laptop:/home/vaporup/
  vaporup@laptop.local:/home/vaporup/
  ~~~~
