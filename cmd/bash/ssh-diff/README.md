## ssh-diff
Diff a file over SSH

  ~~~~
  $ ssh-diff /etc/hosts kim

  Comparing kim:/etc/hosts (<) with /etc/hosts (>)

  1,2c1,2
  < 127.0.0.1 localhost
  < 127.0.1.1 kim
  ---
  > 127.0.0.1 localhost
  > 127.0.1.1 blinky
  ~~~~

### Demo
![](demo.gif)
