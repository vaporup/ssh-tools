module codeberg.org/vaporup/ssh-tools

go 1.21.5

require (
	golang.org/x/crypto v0.20.0
	sigs.k8s.io/yaml v1.4.0
)

require golang.org/x/sys v0.17.0 // indirect
