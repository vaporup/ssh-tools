<h1 align="center"><code>ssh-tools</code></h1>
<p align="center">Making SSH more convenient</p>
<p align="center">
  <img src="https://img.shields.io/badge/uses-ssh-blue.svg?style=flat-square"/>
  <img src="https://img.shields.io/badge/license-gpl--3-brightgreen.svg?style=flat-square"/>
  <img src="https://img.shields.io/badge/status-stable-ff69b4.svg?style=flat-square"/>
  <a href="https://codeberg.org/vaporup/CISS">
   <img src="https://img.shields.io/badge/supports-CISS-blueviolet.svg?style=flat-square"/>
  </a>
  <img src="https://img.shields.io/badge/implementation-bash / perl-red.svg?style=flat-square"/>
</p>

<div align="center">

| Tool                                              | Description
| ------------------------------------------------: | :----------------------------------------------------
| [ssh-ping](cmd/bash/ssh-ping)                     | Check if host is reachable using ```ssh_config```
| [ssh-last](cmd/perl/ssh-last)                     | Like ```last``` but for SSH sessions
| [ssh-certinfo](cmd/bash/ssh-certinfo)             | Shows validity and information of SSH certificates
| [ssh-force-password](cmd/bash/ssh-force-password) | Enforces password authentication
| [ssh-keyinfo](cmd/bash/ssh-keyinfo)               | Prints keys in several formats
| [ssh-hostkeys](cmd/bash/ssh-hostkeys)             | Prints server host keys in several formats
| [ssh-facts](cmd/bash/ssh-facts)                   | Get some facts about the remote system
| [ssh-diff](cmd/bash/ssh-diff)                     | Diff a file over SSH
| [ssh-version](cmd/bash/ssh-version)               | Shows version of the SSH server you are connecting to
| [ssh-pwd](cmd/bash/ssh-pwd)                       | Quickly echo path to use for scp, rsync
| [ssh-authorized-keys](cmd/go/ssh-authorized-keys) | Collects info from ```authorized_keys``` files from every user it can find
| [ssh-sig](cmd/go/ssh-sig)                         | Make ```ssh-keygen -Y``` easier to use

[![Packaging status](https://repology.org/badge/vertical-allrepos/ssh-tools.svg?columns=4)](https://repology.org/project/ssh-tools/versions)
</div>
